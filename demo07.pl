#!/usr/bin/perl

# by Andrew Gosali z3438251
# for perl2python COMP2041 S2 2013 assignment

print "Enter a number: ";
$number = <STDIN>;
if ($number < 0) {
    print "it is a negative number\n";
}
elsif ($number == 0) {
    print "it is zero\n";
}
else {
    print "it is a positive number\n";
}

