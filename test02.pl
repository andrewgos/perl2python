#!/usr/bin/perl

# written by Andrew Gosali z3438251
# for testing perl2python.pl COMP2041 S2 2013 assignment
# testing (=~) function

$a = "hello world";

if ($a =~ /^hello.*/){
    print "$a\n";
}
