#!/usr/bin/perl

# written by Andrew Gosali z3438251
# for testing perl2python.pl COMP2041 S2 2013 assignment
# testing while loops

$a = 1;

while ($a < 10){
    print "$a\n";
    $a ++;
}
